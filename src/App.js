import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Routing from "./Routing/Routing";
import { ChakraProvider } from "@chakra-ui/react";

function App() {
  return (
    <div className="App">
      <ChakraProvider>
        <Router>
          <Routing />
        </Router>
      </ChakraProvider>
    </div>
  );
}

export default App;
