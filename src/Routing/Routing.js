import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";

//SubComponent
import Menubar from "./Components/Menubar";

//Page import
import Home from "../Pages/Home/Index";
// import Explore from "../Pages/Explore/Index";
// import Metadrop from "../Pages/Metadrop/Index";
// import Exchange from "../Pages/Exchange/Index";
import GameFi from "../Pages/GameFi/Index";
import NFTMarket from "../Pages/NFTMarket/Index";
import Login from "../Pages/Login/Index";
import Register from "../Pages/Rgister/Index";

export default function Routing() {
  return (
    <div>
      <div>
        <Menubar />
      </div>
      <div>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Home" element={<Home />} />
          {/* <Route path="/Explore" element={<Explore />} />
          <Route path="/Metadrop" element={<Metadrop />} />
          <Route path="/Exchange" element={<Exchange />} /> */}
          <Route path="/GameFi" element={<GameFi />} />
          <Route path="/NFTMarket" element={<NFTMarket />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Register" element={<Register />} />
        </Routes>
      </div>
    </div>
  );
}
