import React from "react";
// import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {
  Box,
  Flex,
  Text,
  IconButton,
  Button,
  Stack,
  Link,
  Collapse,
  Icon,
  Image,
  Popover,
  PopoverTrigger,
  PopoverContent,
  useColorModeValue,
  useBreakpointValue,
  useDisclosure,
  Container,
} from "@chakra-ui/react";
import {
  HamburgerIcon,
  CloseIcon,
  ChevronDownIcon,
  ChevronRightIcon,
} from "@chakra-ui/icons";
import { SocialIcon } from "react-social-icons";

//img
import XLAFF_LOGO from "../../Assets/img/XLAFF-Icon.png";

export default function WithSubnavigation() {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Box position="fixed" width="100%" zIndex={1}>
      <Flex
        bg="black"
        color="white"
        minH={"5vh"}
        py={{ base: 2 }}
        px={{ base: 4 }}
        align={"center"}
        // fontFamily={"aAtmospheric"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.lg",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
        >
          <Flex
            flex={{ base: 1 }}
            justify={{ base: "space-between", xl: "space-between" }}
          >
            <Flex alignItems={"center"}>
              <Image height="3vh" src={XLAFF_LOGO} />
              <Link
                color={"#fff"}
                p={2}
                fontSize={"sm"}
                fontWeight={800}
                _hover={{
                  textDecoration: "none",
                  color: "blue",
                }}
                fontSize={{ base: "3vw", md: "2.5vw", xl: "1vw" }}
                href="/Home"
              >
                XLAFF
              </Link>
              <Flex
                display={{ base: "none", xl: "flex" }}
                alignItems={"center"}
                ml={5}
              >
                <DesktopNav />
              </Flex>
            </Flex>
            <Stack
              display={{ base: "none", md: "inline-flex", xl: "inline-flex" }}
              justify={"flex-end"}
              direction={"row"}
              spacing={3}
              ml={10}
              alignItems={"center"}
            >
              <Box w={"full"}>
                <Flex justify={"center"}>
                  <SocialIcon
                    url="https://web.telegram.org/k/"
                    bgColor="#32ADE0"
                    fgColor="#fff"
                    style={{
                      marginRight: "0.6rem",
                      width: "1.5rem",
                      height: "1.5rem",
                    }}
                  />
                  <SocialIcon
                    url="https://twitter.com/jaketrent"
                    bgColor="#30A1F2"
                    fgColor="#fff"
                    style={{
                      marginRight: "0.6rem",
                      width: "1.5rem",
                      height: "1.5rem",
                    }}
                  />
                  <SocialIcon
                    url="https://discord.com/"
                    bgColor="#8C9FFF"
                    fgColor="#fff"
                    style={{
                      marginRight: "0.6rem",
                      width: "1.5rem",
                      height: "1.5rem",
                    }}
                  />
                </Flex>
              </Box>
              <Link href="/Login">
                <Link
                  p={2}
                  fontWeight={800}
                  color={"white"}
                  _hover={{
                    textDecoration: "none",
                    color: "blue",
                  }}
                >
                  Login
                </Link>
              </Link>
              <Link href="/Register">
                <Button
                  // display={{ base: "none", md: "inline-flex" }}
                  size={"sm"}
                  fontWeight={800}
                  color={"white"}
                  // variant={"link"}
                  bg={"blue"}
                  href={"#"}
                  _hover={{
                    textDecoration: "none",
                    // color: "gray.800",
                    background: "#070A46",
                  }}
                >
                  Register
                </Button>
              </Link>
            </Stack>
          </Flex>
        </Container>
        <Flex display={{ base: "flex", xl: "none" }}>
          <IconButton
            onClick={onToggle}
            icon={
              isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />
            }
            variant={"ghost"}
            aria-label={"Toggle Navigation"}
          />
        </Flex>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
}

const DesktopNav = () => {
  return (
    <Stack direction={"row"} spacing={2}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={"hover"} placement={"bottom-start"}>
            <PopoverTrigger>
              <Link
                p={2}
                href={navItem.href ?? "#"}
                fontSize={"sm"}
                fontWeight={800}
                color={"white"}
                _hover={{
                  textDecoration: "none",
                  color: "blue",
                }}
              >
                {navItem.label}
              </Link>
            </PopoverTrigger>
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};

const MobileNav = () => {
  return (
    <Flex w="100%" justify={"end"}>
      <Stack
        bg="black"
        p="0rem 1.2rem"
        display={{ xl: "none" }}
        height="100vh"
        width={{ base: "40vw", sm: "30vw" }}
        fontSize={{ base: "3vw", sm: "2vw" }}
      >
        {NAV_ITEMS.map((navItem) => (
          <MobileNavItem key={navItem.label} {...navItem} />
        ))}
        <Stack display={{ base: "block", md: "none", xl: "none" }}>
          <Link href="/Login">
            <Link
              p={2}
              fontWeight={800}
              color={"white"}
              _hover={{
                textDecoration: "none",
                color: "blue",
              }}
            >
              Login
            </Link>
          </Link>
          <Link href="/Register">
            <Button
              // display={{ base: "none", md: "inline-flex" }}
              size={"sm"}
              fontWeight={800}
              color={"white"}
              // variant={"link"}
              width={"100%"}
              mt={1}
              bg={"blue"}
              href={"#"}
              _hover={{
                textDecoration: "none",
                // color: "gray.800",
                background: "#070A46",
              }}
            >
              Register
            </Button>
          </Link>
        </Stack>
      </Stack>
    </Flex>
  );
};

const MobileNavItem = ({ label, children, href }) => {
  const { isOpen, onToggle } = useDisclosure();

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Flex
        py={2}
        as={Link}
        href={href ?? "#"}
        justify={"space-between"}
        align={"center"}
        _hover={{
          textDecoration: "none",
        }}
      >
        <Text fontWeight={600} color="white">
          {label}
        </Text>
        {children && (
          <Icon
            as={ChevronDownIcon}
            transition={"all .25s ease-in-out"}
            transform={isOpen ? "rotate(180deg)" : ""}
            w={6}
            h={6}
          />
        )}
      </Flex>
    </Stack>
  );
};

const NAV_ITEMS = [
  // {
  //   label: "HOME",
  //   href: "/Home",
  // },
  // {
  //   label: "EXPLORE",
  //   href: "/Explore",
  // },
  // {
  //   label: "METADROP",
  //   href: "/Metadrop",
  // },
  // {
  //   label: "EXCHANGE",
  //   href: "/Exchange",
  // },
  {
    label: "GameFI",
    href: "/GameFi",
  },
  {
    label: "NFT Marketplace",
    href: "/NFTMarket",
  },
];
