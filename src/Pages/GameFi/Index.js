import React from "react";
import { Flex, Text, Box, Container, Image } from "@chakra-ui/react";
import { MinusIcon } from "@chakra-ui/icons";
import { SocialIcon } from "react-social-icons";
import Slider from "react-slick";

//img
import SLIDE_1 from "../../Assets/img/SLIDE-1.png";
import SLIDE_2 from "../../Assets/img/SLIDE-2.jpg";
import SLIDE_3 from "../../Assets/img/SLIDE-3.png";
import Sub_SLIDE_3 from "../../Assets/img/SubSLIDE-3.jpg";

//Sub Components
import GameBlock from "./Components/GameBlock";

export default function Index() {
  const settings = {
    customPaging: function (i) {
      return <MinusIcon w={8} h={6} color="#B3B3B3" mb={20} />;
    },
    dots: true,
    arrows: false,
    fade: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <Box w={"full"} paddingTop="5vh">
      <Box w={"full"} bg={"#fff"}>
        <Slider {...settings}>
          <Box
            w={"full"}
            h={{ base: "55vw", md: "55vw", xl: "50vw" }}
            backgroundImage={SLIDE_1}
            backgroundSize={{ base: "cover", md: "cover", xl: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
          <Box
            w={"full"}
            h={{ base: "55vw", md: "55vw", xl: "50vw" }}
            backgroundImage={SLIDE_2}
            backgroundSize={{ base: "cover", md: "cover", xl: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
          <Box
            w={"full"}
            h={{ base: "55vw", md: "55vw", xl: "50vw" }}
            backgroundImage={SLIDE_3}
            backgroundSize={{ base: "cover", md: "cover", xl: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          >
            <Container
              maxW={{
                base: "container.sm",
                md: "container.md",
                xl: "container.lg",
                "2xl": "container.xl",
              }}
              pt={{ base: "30vw", md: "30vw" }}
              // pb={{ base: "2rem", md: "5rem" }}
            >
              <Flex justify={"end"}>
                <Image
                  width={{ base: "50px", sm: "70px", md: "80px", xl: "120px" }}
                  borderRadius="10px"
                  objectFit="cover"
                  src={Sub_SLIDE_3}
                  ml={2}
                />
                <Image
                  width={{ base: "50px", sm: "70px", md: "80px", xl: "120px" }}
                  borderRadius="10px"
                  objectFit="cover"
                  src={Sub_SLIDE_3}
                  ml={2}
                />
                <Image
                  width={{ base: "50px", sm: "70px", md: "80px", xl: "120px" }}
                  borderRadius="10px"
                  objectFit="cover"
                  src={Sub_SLIDE_3}
                  ml={2}
                />
                <Image
                  width={{ base: "50px", sm: "70px", md: "80px", xl: "120px" }}
                  borderRadius="10px"
                  objectFit="cover"
                  src={Sub_SLIDE_3}
                  ml={2}
                />
              </Flex>
            </Container>
          </Box>
        </Slider>
      </Box>
      <Box w={"full"} bg={"#fff"} textAlign={"start"} pb={"2rem"}>
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <GameBlock topic="Top Favorites" />
          <GameBlock topic="All Game" />
          <GameBlock topic="News" />
        </Container>
      </Box>
      <Box w={"full"} bg={"#000"} textAlign={"start"} pb={"2rem"}>
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "2.5vw", md: "2vw", xl: "1vw" }}
            mb={{ base: "0.5rem", md: "1rem" }}
            textAlign={"center"}
          >
            <Text>Join us</Text>
          </Text>
          <Box w={"full"}>
            <Flex justify={"center"}>
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#32ADE0"
                fgColor="#fff"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#30A1F2"
                fgColor="#fff"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#8C9FFF"
                fgColor="#fff"
                style={{
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
            </Flex>
          </Box>
          <Text
            color={"grey"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "1.5vw", md: "1.5vw", xl: "0.8vw" }}
            mt={{ base: "0.5rem", md: "1rem" }}
            textAlign={"center"}
          >
            <Text>XLAFF © 2022</Text>
          </Text>
        </Container>
      </Box>
    </Box>
  );
}
