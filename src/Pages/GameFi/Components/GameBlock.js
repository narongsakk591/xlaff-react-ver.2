import React from "react";
import { Flex, Text, Image, SimpleGrid, Box } from "@chakra-ui/react";

import Game1 from "../../../Assets/img/game1.jpg";
import Game2 from "../../../Assets/img/game2.jpg";
import Game3 from "../../../Assets/img/game3.jpg";
import Game4 from "../../../Assets/img/game4.jpg";
import Game5 from "../../../Assets/img/game6.jpg";

export default function TopFaverite(props) {
  return (
    <>
      <Text
        color={"blue"}
        // fontFamily={"Montserrat"}
        fontSize={{ base: "4vw", md: "3vw", xl: "1.8vw" }}
        mb={{ base: "1.6rem", md: "2rem" }}
        textAlign={"start"}
      >
        <Text>{props.topic}</Text>
      </Text>
      <SimpleGrid
        columns={{ base: 1, md: 2, xl: 5 }}
        spacing={{ base: 6, md: 10, xl: 7 }}
        px={{ base: 3, sm: 5, md: 3, xl: 14 }}
        pb={{ base: "3rem", md: "5rem", xl: "7rem" }}
      >
        <Box textAlign={"center"}>
          <Flex justify={"center"}>
            <Image
              width={{ base: "60%", sm: "70%", md: "100%", xl: "100%" }}
              borderRadius="15%"
              objectFit="cover"
              border={"3px solid blue"}
              boxShadow={"3px 3px 5px #888888"}
              src={Game1}
              mb={{ base: "0.5rem", md: "1rem" }}
            />
          </Flex>
          <Text
            color={"#000"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3vw", xl: "1vw" }}
          >
            <Text>Axie Infinity</Text>
          </Text>
        </Box>
        <Box textAlign={"center"}>
          <Flex justify={"center"}>
            <Image
              width={{ base: "60%", sm: "70%", md: "100%", xl: "100%" }}
              borderRadius="15%"
              objectFit="cover"
              border={"3px solid blue"}
              boxShadow={"3px 3px 5px #888888"}
              src={Game2}
              mb={{ base: "0.5rem", md: "1rem" }}
            />
          </Flex>
          <Text
            color={"#000"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1vw" }}
          >
            <Text>Splinterlands</Text>
          </Text>
        </Box>
        <Box textAlign={"center"}>
          <Flex justify={"center"}>
            <Image
              width={{ base: "60%", sm: "70%", md: "100%", xl: "100%" }}
              borderRadius="15%"
              objectFit="cover"
              border={"3px solid blue"}
              boxShadow={"3px 3px 5px #888888"}
              src={Game3}
              mb={{ base: "0.5rem", md: "1rem" }}
            />
          </Flex>
          <Text
            color={"#000"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1vw" }}
          >
            <Text>Gods Unchained</Text>
          </Text>
        </Box>
        <Box textAlign={"center"}>
          <Flex justify={"center"}>
            <Image
              width={{ base: "60%", sm: "70%", md: "100%", xl: "100%" }}
              borderRadius="15%"
              objectFit="cover"
              border={"3px solid blue"}
              boxShadow={"3px 3px 5px #888888"}
              src={Game4}
              mb={{ base: "0.5rem", md: "1rem" }}
            />
          </Flex>
          <Text
            color={"#000"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1vw" }}
          >
            <Text>MetaGods</Text>
          </Text>
        </Box>
        <Box textAlign={"center"}>
          <Flex justify={"center"}>
            <Image
              width={{ base: "60%", sm: "70%", md: "100%", xl: "100%" }}
              borderRadius="15%"
              objectFit="cover"
              border={"3px solid blue"}
              boxShadow={"3px 3px 5px #888888"}
              src={Game5}
              mb={{ base: "0.5rem", md: "1rem" }}
            />
          </Flex>
          <Text
            color={"#000"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1vw" }}
          >
            <Text>Lost Relics</Text>
          </Text>
        </Box>
      </SimpleGrid>
    </>
  );
}
