import React from "react";
import {
  Flex,
  Text,
  Image,
  SimpleGrid,
  Box,
  Container,
} from "@chakra-ui/react";
import { SocialIcon } from "react-social-icons";

//img
import HOME_BG_1 from "../../Assets/img/HOME-BG-1.jpg";
import HOME_BG_2 from "../../Assets/img/HOME-BG-2.jpg";
import HOME_BG_2_1 from "../../Assets/img/HOME-BG-2-1.png";
import HOME_BG_3 from "../../Assets/img/HOME-BG-3.jpg";
import HOME_BG_4 from "../../Assets/img/HOME-BG-4.jpg";
import HOME_BG_4_1 from "../../Assets/img/HOME-BG-4-1.jpg";

//Sub components
import Carousel from "./Components/Carousel";

export default function Index() {
  return (
    <Box w={"full"} paddingTop="0vh">
      <Box
        w={"full"}
        // h={"80vh"}
        backgroundImage={HOME_BG_1}
        backgroundSize={{ base: "cover" }}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"center"}
        align="end"
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "2rem", md: "5rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Carousel />
          <Text
            color={"#fff"}
            fontFamily={"Montserrat"}
            fontWeight={900}
            fontSize={{ base: "4vw", md: "4vw", xl: "1.8vw" }}
            pt="60vh"
            pb="5vh"
          >
            <Text>CONNECTING A GOOD LIFE</Text>
            <Text mt={"-1vh"}>TO COMMUNITY</Text>
          </Text>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        backgroundImage={HOME_BG_2}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Flex
            flex={{ base: 1 }}
            justify={{ base: "space-between", xl: "space-between" }}
            align={"center"}
            ml={{ base: "0.5rem", md: "2rem" }}
            mr={{ base: "0.5rem", md: "2rem" }}
            pb={{ base: "0.5rem", md: "4rem" }}
          >
            <Box>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "3vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>FROM EXPERIMENT</Text>
                <Text mt={"-1vh"}>TO EXPONENTIAL</Text>
                <Text mt={"-1vh"}>GROWTH</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
                width={"75%"}
              >
                <Text>
                  We are excited to share the news with XLAFF today that XLAFF
                  has completed a US$6 million seed round, co-led by DeFiance
                  Capital and Hashed. We also received backing from Pantera
                  Capital, Coinbase Ventures, Alameda Research, Animoca Brands,
                  Dapper Labs, Play Ventures, Coin98 Ventures and SkyVision
                  Capital.
                </Text>
              </Text>
            </Box>
            <Flex w={"full"} justify={"end"}>
              <Image
                width={{ base: "70%", sm: "60%", lg: "85%", xl: "40%" }}
                src={HOME_BG_2_1}
              />
            </Flex>
          </Flex>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        backgroundImage={HOME_BG_3}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"start"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          pb={{ base: "8rem", md: "12rem", xl: "17rem", "2xl": "21rem" }}
        >
          <Box width={"full"} textAlign={"center"}>
            <Text
              color={"#fff"}
              fontFamily={"Montserrat"}
              fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
            >
              <Text>WHAT WE DO</Text>
            </Text>
            <Text
              color={"#fff"}
              fontFamily={"Montserrat"}
              fontSize={{ base: "3.5vw", md: "3.5vw", xl: "3vw" }}
              mb={{ base: "0.5rem", md: "2rem" }}
            >
              <Text>INVESTMENT</Text>
              <Text mt={"-1vh"}>IN EXPONENTIAL</Text>
              <Text mt={"-1vh"}>OPPORTUNITIES</Text>
            </Text>
          </Box>
        </Container>
      </Box>
      <Box
        w={"full"}
        // h={"100vh"}
        backgroundImage={HOME_BG_4}
        backgroundSize={"cover"}
        backgroundRepeat={"no-repeat"}
        backgroundPosition={"center center"}
        textAlign={"start"}
        pb={"2rem"}
      >
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "3.5vw", md: "3.5vw", xl: "3vw" }}
            mb={{ base: "0.5rem", md: "2rem" }}
            textAlign={"center"}
          >
            <Text>NEWS</Text>
          </Text>
          <SimpleGrid
            columns={{ base: 1, md: 2, xl: 3 }}
            spacing={{ base: 6, md: 10, xl: 14 }}
            px={{ base: 3, sm: 5, md: 3, xl: 14 }}
          >
            <Box>
              <Flex w={"full"} justify={"center"} mb={"1rem"}>
                <Image
                  width={{
                    base: "100%",
                  }}
                  src={HOME_BG_4_1}
                />
              </Flex>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Sample Headline</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  Sample text. Click to select the text box. Click again or
                  double click to start editing the text.
                </Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Read more</Text>
              </Text>
            </Box>
            <Box>
              <Flex w={"full"} justify={"center"} mb={"1rem"}>
                <Image
                  width={{
                    base: "100%",
                  }}
                  src={HOME_BG_4_1}
                />
              </Flex>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Sample Headline</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  Sample text. Click to select the text box. Click again or
                  double click to start editing the text.
                </Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Read more</Text>
              </Text>
            </Box>
            <Box>
              <Flex w={"full"} justify={"center"} mb={"1rem"}>
                <Image
                  width={{
                    base: "100%",
                  }}
                  src={HOME_BG_4_1}
                />
              </Flex>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Sample Headline</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  Sample text. Click to select the text box. Click again or
                  double click to start editing the text.
                </Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Read more</Text>
              </Text>
            </Box>
          </SimpleGrid>
        </Container>
      </Box>
      <Box w={"full"} bg={"#14141D"} textAlign={"start"} pb={"2rem"}>
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "2.5vw", md: "2.5vw", xl: "2vw" }}
            mb={{ base: "0.5rem", md: "2rem" }}
            textAlign={"start"}
          >
            <Text>Get in touch. Stay in touch.</Text>
          </Text>
          <SimpleGrid
            columns={{ base: 1, md: 2, xl: 3 }}
            spacing={{ base: 6, md: 10, xl: 14 }}
            px={{ base: 3, sm: 5, md: 3, xl: 14 }}
          >
            <Box textAlign={"center"}>
              <Flex justify={"center"}>
                <Box
                  w={"30%"}
                  mb={"1rem"}
                  bg={"#2D8CFC"}
                  borderRadius={"50%"}
                  p={"0.7vw"}
                >
                  <svg
                    class="u-svg-content"
                    viewBox="0 0 512 512"
                    id="svg-bfbd"
                  >
                    <g>
                      <path
                        clip-rule="evenodd"
                        d="M480.917,488.672c-0.846-27.915-35.781-73.555-66.076-77.553   c-29.928-3.868-51.98-19.994-69.604-35.247c-4.31-3.731-8.196-5.532-11.871-5.532c-4.365,0-8.225,2.5-11.799,7.664   c-9.52,13.746-22.136,24.764-34.338,35.432c-6.35,5.55-12.92,11.283-18.975,17.339c-2.288,2.241-4.172,3.28-5.932,3.28   c-1.677,0-3.386-0.975-5.219-2.986c-6.304-6.947-13.82-12.689-21.093-18.24c-4.355-3.326-8.711-6.652-12.851-10.254   c-20.601-17.881-29.151-38.032-26.137-61.619c0.092-0.754,0.092-1.59,0.092-2.436c-0.005-1.645-0.005-3.354,0.79-4.144   c0.418-0.423,1.084-0.625,2.04-0.625c0.404,0,0.854,0.036,1.355,0.11c1.589,0.257,3.147,0.432,4.613,0.432   c5.086,0,8.288-2.215,9.786-6.771c2.513-7.885-3.051-12.093-7.962-15.796l-0.96-0.726c-20.059-15.125-35.519-35.046-47.262-60.889   c-7.783-17.123-8.577-34.283-2.356-51.011c9.91-27.106,22.094-45.185,40.108-59.083c2.954-2.307,4.737-3.698,6.35-3.698   c1.447,0,3.133,1.121,6.019,4.006c39.442,39.14,76.15,63.972,115.524,78.146c9.28,3.354,12.166,8.348,10.282,17.816l-1.213,6.474   c-4.466,24.006-8.692,46.684-28.163,63.683c-2.711,2.353-4.282,5.292-4.42,8.306c-0.129,2.702,0.946,5.312,3.087,7.526   c2.04,2.113,4.402,3.188,7.021,3.188c2.794,0,5.789-1.213,8.94-3.603c10.963-8.306,18.966-19.066,23.79-31.977   c6.965-18.515,11.1-35.96,12.644-53.34c0.569-6.869,1.332-11.224,7.186-13.848c5.045-2.229,7.148-6.749,5.623-12.102   c-1.286-4.401-4.769-6.938-9.795-7.144c-25.049-1.117-46.183-11.665-70.22-27.153c-42.406-27.304-73.992-53.992-90.316-95.512   c-2.628-6.822-6.147-10.135-10.764-10.135c-1.512,0-3.147,0.372-4.861,1.107c-3.037,1.313-5.081,3.285-6.069,5.857   c-1.227,3.202-0.767,7.319,1.333,11.899c3.101,6.768,6.662,13.411,10.434,20.449c1.769,3.299,3.593,6.699,5.453,10.269   c-5.715,1.466-11.633,3.469-16.801,7.535c-26.473,20.638-41.533,49.058-46.04,86.897c-4.057,33.746,13.024,61.077,32.091,85.583   c11.721,15.088,10.838,32.197,9.984,48.736c-0.446,8.583-0.901,17.468,0.386,26.06c0.078,0.423-1.021,1.543-1.682,2.215   c-0.565,0.578-1.135,1.166-1.558,1.736c-11.651,15.859-25.659,30.745-49.059,32.289c-5.94,0.349-11.835,2.113-17.536,3.822   c-4.149,1.25-8.44,2.536-12.676,3.253c-27.028,4.429-45.08,20.408-55.183,48.847c-0.579,1.627-1.346,3.382-2.141,5.183   c-2.504,5.706-5.338,12.166-3.229,17.22c1.08,2.591,3.386,4.594,6.859,5.918c1.709,0.661,3.308,0.982,4.75,0.982   c7.388,0,9.717-8.021,11.775-15.097c0.579-1.985,1.135-3.915,1.783-5.55c8.003-20.326,21.139-32.133,40.154-36.094   c9.975-2.076,19.884-4.502,29.79-6.909c5.895-1.434,11.789-2.867,17.697-4.245c2.095-0.479,3.979-0.717,5.757-0.717   c5.127,0,9.331,2.003,13.236,6.303c9.763,10.824,19.985,21.483,29.867,31.812c5.288,5.513,10.581,11.036,15.833,16.613   c6.662,7.085,11.688,10.107,16.811,10.107c4.916,0,10.089-2.895,16.769-9.409l1.897-1.847c7.471-7.323,11.587-11.357,14.854-11.357   c3.501,0,7.007,4.732,13.99,14.179l1.268,1.709c1.654,2.315,3.611,4.925,6.064,6.846c2.573,1.957,5.605,3.087,8.325,3.087   c2.757,0,4.98-1.112,6.469-3.226c5.716-8.38,13.526-15.115,21.098-21.63c9.409-8.104,19.149-16.484,24.827-28.081   c6.405-13.111,14.941-19.223,26.841-19.223c4.181,0,8.757,0.754,13.976,2.279c2.904,0.873,5.955,1.387,8.821,1.811   c36.838,4.86,58.918,25.957,67.519,64.514c1.305,5.99,3.73,12.846,11.018,12.846c0.726,0,1.497-0.064,2.324-0.212   C481.284,503.337,481.045,494.158,480.917,488.672z M341.064,403.548l-0.826,1.746c-0.515,1.112-1.029,2.233-1.7,3.299   c-12.313,20.583-27.189,38.133-44.188,52.164c-2.324,1.902-4.153,2.794-5.771,2.794c-1.7,0-3.372-0.993-5.274-3.125   c-3.979-4.502-6.239-7.811-1.488-12.479c6.487-6.294,13.488-12.285,20.27-18.083c10.439-8.931,21.227-18.156,30.488-28.761   c1.121-1.277,2.261-1.93,3.391-1.93C337.877,399.174,339.77,401.104,341.064,403.548z M220.624,462.613   c-0.556-0.699-1.103-1.379-1.649-1.957c-5.623-5.835-11.155-11.799-16.687-17.762c-7.539-8.123-15.083-16.256-22.884-24.111   c-2.826-2.821-4.186-5.403-4.149-7.885c0.055-3.795,3.363-6.928,6.565-9.941l1.663-1.599c1.7-1.7,3.193-2.563,4.438-2.563   c2.224,0,3.809,2.912,5.21,5.485c0.569,1.048,1.121,2.049,1.686,2.839c6.841,9.474,16.052,16.411,24.957,23.119   c5.825,4.383,11.849,8.922,17.091,14.096l1.566,1.442c2.32,2.104,4.517,4.089,5.077,6.854c-3.331,5.899-8.266,10.889-15.102,15.253   c-0.864,0.57-1.659,0.846-2.417,0.846C223.946,466.729,222.255,464.635,220.624,462.613z"
                        fill-rule="evenodd"
                      ></path>
                      <path
                        clip-rule="evenodd"
                        d="M99.761,278.315c1.893,0.239,3.639,0.358,5.237,0.358   c10.54,0,15.166-5.394,15.47-18.033c0.437-20.274,0.409-40.182-0.088-59.166c-1.001-33.86,4.406-66.333,16.531-99.27   c8.844-24.189,25.595-43.513,49.78-57.434c19.406-11.082,40.402-16.701,62.409-16.701c53.942,0,102.403,33.759,117.859,82.101   c9.216,28.609,12.9,54.342,11.265,78.784c0.993,21.396-0.423,40.347-4.318,57.93c-4.07,17.762-8.601,33.534-13.838,48.218   c-9.392,26.334-27.033,43.37-52.44,50.62l-1.378,0.396c-1.884,0.551-3.832,1.111-5.56,1.111c-1.947,0-3.308-0.744-4.264-2.361   c-7.874-13.148-19.645-14.922-30.129-14.922c-3.125,0-6.304,0.165-9.487,0.33l-1.902,0.102   c-16.085,0.873-26.372,10.962-27.511,26.986c-1.245,16.117,9.11,29.386,24.625,31.545c3.753,0.524,7.149,0.781,10.375,0.781   c12.629,0,22.415-4.08,29.922-12.479c5.32-5.834,12.553-8.251,18.938-9.85c36.332-9.354,60.011-32.96,72.37-72.168   c2.83-8.913,6.763-12.056,15.574-12.441c21.199-1.048,34.302-11.707,38.979-31.701c5.008-21.451,3.538-43.155-4.373-64.509   c-4.016-10.82-10.273-16.737-18.093-17.114c-13.948-0.85-15.29-9.694-16.853-19.935l-0.221-1.475   c-4.833-31.977-13.084-56.391-26.004-76.836c-19.819-31.636-66.957-65.69-123.601-65.69c-14.1,0-27.952,2.219-41.166,6.598   c-42.792,14.233-69.729,35.688-84.77,67.527c-12.212,25.853-19.779,49.844-23.137,73.344c-1.194,8.293-4.204,11.357-11.913,12.129   c-13.728,1.3-22.577,10.162-24.92,24.956c-1.828,12.185-3.735,26.606-4.011,41.211C58.522,263.476,68.019,274.465,99.761,278.315z    M91.44,188.051l0.533,0.863c4.07,0.335,5.063,3.202,5.343,7.7l0.446,9.193c0.634,13.117,1.268,26.271,1.682,39.355l0.069,1.636   c0.234,4.599,0.055,7.434-4.351,7.962c-0.579,0.074-1.145,0.11-1.695,0.11c-4.604,0-8.15-2.637-9.979-7.411   c-1.897-5.183-1.75-10.416-1.589-15.961c0.069-2.384,0.133-4.773,0.032-7.135c-0.276-10.797,0.717-20.279,3.032-29   c1.185-4.475,3.166-6.469,6.418-6.469v-0.836C91.404,188.06,91.417,188.051,91.44,188.051z M407.278,191.753   c3.097,0,5.422,1.99,6.91,5.922c3.565,9.042,5.477,19.071,5.127,26.872c-0.175,16.958-1.396,29.432-15.676,35.546   c-2.426,1.021-4.264,1.512-5.642,1.512c-1.029,0-1.783-0.279-2.297-0.863c-0.983-1.126-1.26-3.469-0.809-6.97   c0.734-5.55,1.699-11.072,2.655-16.6c2.104-12.175,4.291-24.763,3.997-37.476c-0.092-4.393,0.698-7.416,4.852-7.894   C406.709,191.768,406.994,191.753,407.278,191.753z"
                        fill-rule="evenodd"
                      ></path>
                    </g>
                  </svg>
                </Box>
              </Flex>

              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Support</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  Got a problem? Just get in touch. Our support team is
                  available 24/7.
                </Text>
              </Text>
            </Box>
            <Box textAlign={"center"}>
              <Flex justify={"center"}>
                <Box
                  w={"30%"}
                  mb={"1rem"}
                  bg={"#2D8CFC"}
                  borderRadius={"50%"}
                  p={"0.7vw"}
                >
                  <svg
                    class="u-svg-content"
                    viewBox="0 0 512 512"
                    id="svg-a746"
                  >
                    <path d="M289,327a5.23,5.23,0,0,0-5.24,5.24v62.68a5.24,5.24,0,1,0,10.48,0V332.25A5.23,5.23,0,0,0,289,327Z"></path>
                    <path d="M223.35,327a5.23,5.23,0,0,0-5.24,5.24v62.68a5.24,5.24,0,0,0,10.48,0V332.25A5.23,5.23,0,0,0,223.35,327Z"></path>
                    <path d="M398.85,283.06a5.24,5.24,0,0,0-5.24,5.25v50.14a5.24,5.24,0,0,0,10.48,0V288.31A5.24,5.24,0,0,0,398.85,283.06Z"></path>
                    <path d="M346.32,283.06a5.24,5.24,0,0,0-5.24,5.25v50.14a5.24,5.24,0,0,0,10.48,0V288.31A5.24,5.24,0,0,0,346.32,283.06Z"></path>
                    <path d="M166.06,283.06a5.24,5.24,0,0,0-5.24,5.25v50.14a5.24,5.24,0,0,0,10.48,0V288.31A5.24,5.24,0,0,0,166.06,283.06Z"></path>
                    <path d="M113.53,283.06a5.25,5.25,0,0,0-5.25,5.25v50.14a5.25,5.25,0,0,0,10.49,0V288.31A5.24,5.24,0,0,0,113.53,283.06Z"></path>
                    <path d="M461.82,157.16h-.34a27.62,27.62,0,0,0,6.29-17.42,27.92,27.92,0,0,0-55.83,0,27.61,27.61,0,0,0,6.28,17.42h-.33a24.69,24.69,0,0,0-24.67,24.66v27.69A32.64,32.64,0,0,0,357.54,206V181.82a24.68,24.68,0,0,0-24.65-24.66h-.35a27.62,27.62,0,0,0,6.29-17.42,27.91,27.91,0,1,0-55.82,0,27.61,27.61,0,0,0,6.28,17.42H289a24.68,24.68,0,0,0-24.66,24.66v45a40.73,40.73,0,0,0-8.09-.81,40.16,40.16,0,0,0-18,4.41V181.82a24.68,24.68,0,0,0-24.66-24.66h-.34a27.61,27.61,0,0,0,6.28-17.42,27.91,27.91,0,1,0-55.82,0,27.62,27.62,0,0,0,6.29,17.42h-.34a24.69,24.69,0,0,0-24.67,24.66v20.92a31.31,31.31,0,0,0-26.12,7.1v-28a24.69,24.69,0,0,0-24.67-24.66h-.33a27.61,27.61,0,0,0,6.28-17.42,27.92,27.92,0,0,0-55.83,0,27.62,27.62,0,0,0,6.29,17.42h-.34a24.69,24.69,0,0,0-24.66,24.66v40a5.24,5.24,0,0,0,10.48,0v-40a14.19,14.19,0,0,1,14.18-14.17H94.11a14.19,14.19,0,0,1,14.19,14.17v40a5.11,5.11,0,0,0,.33,1.66,33.07,33.07,0,0,0,6.91,35.37h-3.2a29.55,29.55,0,0,0-29.52,29.52v50a5.24,5.24,0,1,0,10.48,0v-50a19.05,19.05,0,0,1,19-19h54.92a19,19,0,0,1,19,19V394.93a5.25,5.25,0,0,0,10.49,0V332.37a25.13,25.13,0,0,1,25.1-25.09h68.64a25.13,25.13,0,0,1,25.1,25.09v62.56a5.25,5.25,0,0,0,10.49,0V288.41a19,19,0,0,1,19-19h54.92a19,19,0,0,1,19,19v50a5.24,5.24,0,0,0,10.48,0v-50a29.55,29.55,0,0,0-29.51-29.52h-3.2a33.4,33.4,0,0,0,6.67-36.15,5.26,5.26,0,0,0,.17-.88v-40a14.19,14.19,0,0,1,14.19-14.17h43.93A14.19,14.19,0,0,1,476,181.82v40a5.24,5.24,0,1,0,10.48,0v-40A24.69,24.69,0,0,0,461.82,157.16ZM54.72,139.74a17.43,17.43,0,1,1,17.43,17.42A17.45,17.45,0,0,1,54.72,139.74Zm238.77,0a17.43,17.43,0,1,1,17.43,17.42A17.46,17.46,0,0,1,293.49,139.74Zm-7.11,126.88a30.18,30.18,0,1,1-30.18-30.18A30.21,30.21,0,0,1,286.38,266.62ZM174.11,139.74a17.42,17.42,0,1,1,17.42,17.42A17.45,17.45,0,0,1,174.11,139.74Zm-57.4,96.06a23.09,23.09,0,1,1,23.09,23.09A23.12,23.12,0,0,1,116.71,235.8Zm105.17,61a35.49,35.49,0,0,0-25.1,10.39V288.41a29.55,29.55,0,0,0-29.52-29.52h-3.2a33.27,33.27,0,0,0-8.67-52.65V181.82a14.19,14.19,0,0,1,14.18-14.17H213.5a14.18,14.18,0,0,1,14.17,14.17v54a5.2,5.2,0,0,0,.33,1.63,40.28,40.28,0,0,0,1.2,59.36Zm93.74-8.38v18.77a35.49,35.49,0,0,0-25.1-10.39H283.2a40.24,40.24,0,0,0-8.43-66.13V181.82A14.19,14.19,0,0,1,289,167.65h43.94a14.18,14.18,0,0,1,14.17,14.17v32.44a33.08,33.08,0,0,0,1.28,44.63h-3.2A29.55,29.55,0,0,0,315.62,288.41Zm33.89-52.61a23.09,23.09,0,1,1,23.09,23.09A23.12,23.12,0,0,1,349.51,235.8Zm72.91-96.06a17.43,17.43,0,1,1,17.43,17.42A17.46,17.46,0,0,1,422.42,139.74Z"></path>
                  </svg>
                </Box>
              </Flex>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Community</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  XLaff is global. Join the discussion in our worldwide
                  communities.
                </Text>
              </Text>
            </Box>
            <Box textAlign={"center"}>
              <Flex justify={"center"}>
                <Box
                  w={"30%"}
                  mb={"1rem"}
                  bg={"#2D8CFC"}
                  borderRadius={"50%"}
                  p={"0.7vw"}
                >
                  <svg class="u-svg-content" viewBox="0 0 20 20" id="svg-7197">
                    <path
                      d="M8.75 3.75C8.75 2.23122 7.51878 1 6 1C4.48122 1 3.25 2.23122 3.25 3.75C3.25 5.26878 4.48122 6.5 6 6.5C7.51878 6.5 8.75 5.26878 8.75 3.75ZM4.25 3.75C4.25 2.7835 5.0335 2 6 2C6.9665 2 7.75 2.7835 7.75 3.75C7.75 4.7165 6.9665 5.5 6 5.5C5.0335 5.5 4.25 4.7165 4.25 3.75Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M2.5 7.5H6.68252C6.51859 7.81013 6.39687 8.14601 6.32501 8.5H2.5C2.22386 8.5 2 8.72386 2 9V9.5C2 10.7591 3.09851 12.1138 5.09636 12.4309C4.77396 12.6501 4.50546 12.9426 4.31486 13.2845C2.20563 12.7119 1 11.0874 1 9.5V9C1 8.17157 1.67157 7.5 2.5 7.5Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M7.87858 7.5C8.38298 6.88925 9.14603 6.5 10 6.5C10.854 6.5 11.617 6.88925 12.1214 7.5C12.3605 7.78952 12.5415 8.12881 12.6465 8.5C12.7139 8.73842 12.75 8.98999 12.75 9.25C12.75 10.32 12.1389 11.2473 11.2466 11.7019C10.8919 11.8825 10.4929 11.9885 10.0702 11.9991C10.0469 11.9997 10.0235 12 10 12C9.97654 12 9.95315 11.9997 9.92983 11.9991C9.50709 11.9885 9.10806 11.8826 8.75342 11.7019C7.86115 11.2473 7.25 10.32 7.25 9.25C7.25 8.98999 7.28608 8.73842 7.35352 8.5C7.4585 8.12881 7.63948 7.78952 7.87858 7.5ZM8.41841 8.5C8.31042 8.72731 8.25 8.9816 8.25 9.25C8.25 9.96407 8.67768 10.5782 9.29086 10.8504C9.50763 10.9466 9.74757 11 10 11C10.2524 11 10.4924 10.9466 10.7091 10.8504C11.3223 10.5782 11.75 9.96407 11.75 9.25C11.75 8.9816 11.6896 8.72731 11.5816 8.5C11.3362 7.98351 10.8453 7.60627 10.2597 7.51914C10.175 7.50653 10.0883 7.5 10 7.5C9.91175 7.5 9.82502 7.50653 9.74028 7.51914C9.15468 7.60627 8.66377 7.98351 8.41841 8.5Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M15.6851 13.2845C15.4945 12.9426 15.226 12.6501 14.9036 12.4309C16.9015 12.1138 18 10.7591 18 9.5V9C18 8.72386 17.7761 8.5 17.5 8.5H13.675C13.6031 8.14601 13.4814 7.81013 13.3175 7.5H17.5C18.3284 7.5 19 8.17157 19 9V9.5C19 11.0874 17.7944 12.7119 15.6851 13.2845Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M14.4872 13.3706C14.2234 13.1398 13.878 13 13.5 13H6.5C6.06797 13 5.6786 13.1826 5.40489 13.4749C5.15376 13.7431 5 14.1036 5 14.5V15C5 16.9714 6.85951 19 10 19C13.1405 19 15 16.9714 15 15V14.5C15 14.0496 14.8015 13.6456 14.4872 13.3706ZM6 14.5C6 14.2239 6.22386 14 6.5 14H13.5C13.7761 14 14 14.2239 14 14.5V15C14 16.4376 12.5678 18 10 18C7.43216 18 6 16.4376 6 15V14.5Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M14 1C15.5188 1 16.75 2.23122 16.75 3.75C16.75 5.26878 15.5188 6.5 14 6.5C12.4812 6.5 11.25 5.26878 11.25 3.75C11.25 2.23122 12.4812 1 14 1ZM14 2C13.0335 2 12.25 2.7835 12.25 3.75C12.25 4.7165 13.0335 5.5 14 5.5C14.9665 5.5 15.75 4.7165 15.75 3.75C15.75 2.7835 14.9665 2 14 2Z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </Box>
              </Flex>

              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "3.5vw", md: "3.5vw", xl: "1.5vw" }}
                mb={{ base: "0.5rem", md: "1rem" }}
              >
                <Text>Careers</Text>
              </Text>
              <Text
                color={"#fff"}
                fontFamily={"Montserrat"}
                fontSize={{ base: "1.8vw", md: "1.5vw", xl: "1vw" }}
                mb={{ base: "0.5rem", md: "2rem" }}
              >
                <Text>
                  Help build the future of technology. Start your new career at
                  XLaff.
                </Text>
              </Text>
            </Box>
          </SimpleGrid>
        </Container>
      </Box>
      <Box w={"full"} bg={"#000"} textAlign={"start"} pb={"2rem"}>
        <Container
          maxW={{
            base: "container.sm",
            md: "container.md",
            xl: "container.lg",
            "2xl": "container.xl",
          }}
          pt={{ base: "1rem", md: "2rem" }}
          // pb={{ base: "2rem", md: "5rem" }}
        >
          <Text
            color={"#fff"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "2.5vw", md: "2vw", xl: "1vw" }}
            mb={{ base: "0.5rem", md: "1rem" }}
            textAlign={"center"}
          >
            <Text>Join us</Text>
          </Text>
          <Box w={"full"}>
            <Flex justify={"center"}>
              <SocialIcon
                url="https://web.telegram.org/k/"
                bgColor="#32ADE0"
                fgColor="#fff"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://twitter.com/jaketrent"
                bgColor="#30A1F2"
                fgColor="#fff"
                style={{
                  marginRight: "0.6rem",
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
              <SocialIcon
                url="https://discord.com/"
                bgColor="#8C9FFF"
                fgColor="#fff"
                style={{
                  width: "1.5rem",
                  height: "1.5rem",
                }}
              />
            </Flex>
          </Box>
          <Text
            color={"grey"}
            fontFamily={"Montserrat"}
            fontSize={{ base: "1.5vw", md: "1.5vw", xl: "0.8vw" }}
            mt={{ base: "0.5rem", md: "1rem" }}
            textAlign={"center"}
          >
            <Text>XLAFF © 2022</Text>
          </Text>
        </Container>
      </Box>
    </Box>
  );
}
