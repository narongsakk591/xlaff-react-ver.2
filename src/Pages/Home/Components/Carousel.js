import React from "react";
import "./Carousel.css";
import {
  Flex,
  Text,
  Image,
  SimpleGrid,
  Box,
  Container,
} from "@chakra-ui/react";
//img
import Slide3D_1 from "../../../Assets/img/01.jpg";
import Slide3D_2 from "../../../Assets/img/02.jpg";
import Slide3D_3 from "../../../Assets/img/03.jpg";
import Slide3D_4 from "../../../Assets/img/04.jpg";
import Slide3D_5 from "../../../Assets/img/05.jpg";
import Slide3D_6 from "../../../Assets/img/06.jpg";

export default function Carousel() {
  return (
    <Box
      id="contentContainer"
      className="trans3d"
      top={{ base: "11%", sm: "10%", md: "12%", lg: "14%", xl: "11%" }}
    >
      <Box id="carouselContainer" className="trans3d">
        <Box id="item1" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_1}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
        <Box id="item2" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_2}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
        <Box id="item3" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_3}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
        <Box id="item4" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_4}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
        <Box id="item5" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_5}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
        <Box id="item5" className="carouselItem trans3d">
          <Box
            className="carouselItemInner trans3d"
            backgroundImage={Slide3D_6}
            backgroundSize={{ base: "cover" }}
            backgroundRepeat={"no-repeat"}
            backgroundPosition={"center center"}
          ></Box>
        </Box>
      </Box>
    </Box>
  );
}
